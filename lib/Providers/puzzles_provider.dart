import 'package:http/http.dart' as http;
import 'package:riddly/models/dlc.dart';
import 'dart:async';
import 'dart:convert';

import '../models/multiple_choice_puzzle.dart';
import '../models/dlc.dart';

Future<List<MultipleChoicePuzzle>> fetchLevelData() async {
  final levelsResponse =
      await http.get('https://riddly-cfcc8.firebaseio.com/level.json');

  if (levelsResponse.statusCode == 200) {
    // If server returns an OK response, parse the JSON.
    print(json.decode(levelsResponse.body));

    final List<MultipleChoicePuzzle> loadedLevels = [];
    final extractedData =
        json.decode(levelsResponse.body) as Map<String, dynamic>;

    extractedData.forEach((levelId, levelData) {
      loadedLevels.add(MultipleChoicePuzzle(
        id: levelId,
        dlcId: levelData['dlcId'],
        created: levelData['created'],
        name: levelData['name'],
        description: levelData['description'],
        uncorrectAnswers: levelData['uncorrectAnswers'].cast<String>(),
        correctAnswers: levelData['correctAnswers'].cast<String>(),
      ));
    });

    return loadedLevels;
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load puzzles');
  }
}

Future<List<PuzzleDlc>> fetchDlcData() async {
  final dlcResponse =
      await http.get('https://riddly-cfcc8.firebaseio.com/dlc.json');

  if (dlcResponse.statusCode == 200) {
    // If server returns an OK response, parse the JSON.
    print(json.decode(dlcResponse.body));
    final List<PuzzleDlc> loadedDlc = [];

    final dlcData = json.decode(dlcResponse.body) as Map<String, dynamic>;

    dlcData.forEach((levelId, levelData) {
      loadedDlc.add(PuzzleDlc(
        created: levelData['created'],
        description: levelData['description'],
        dlcId: levelData['dlcId'],
        name: levelData['name'],
        price: levelData['price'],
      ));
    });

    return loadedDlc;
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load puzzles');
  }
}
