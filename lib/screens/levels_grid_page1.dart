import 'dart:ui';

import 'package:flutter/material.dart';


class LevelsGrid extends StatefulWidget {
  @override
  LevelsGridState createState() => LevelsGridState();
}

class LevelsGridState extends State<LevelsGrid> {
  int puzzleIndex;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(),
      body: GridView.count(
        crossAxisCount: 2,
        children: List.generate(20, (index) {
          
          return Center(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(15)),
                boxShadow: [BoxShadow(
                  offset: Offset(0, 3),
                  color: Colors.black12,
                  blurRadius: 15,
                )]
              ),
              width: MediaQuery.of(context).size.width * 0.3,
              height: MediaQuery.of(context).size.height * 0.17,
              child: FlatButton(
                onPressed: (){  
                  puzzleIndex = index;        
                  //Navigator.push(context, MaterialPageRoute(builder: (context) => SolvePuzzlePage(index)));
                },
                child: Text('${index+1}'),
              ),
            )
          );
        }),
      ),
    );
  }
}
