import 'package:flutter/material.dart';
import 'package:riddly/models/dlc.dart';

import '../widgets/level_pack.dart';

class LevelsMenu extends StatelessWidget {
  final List<PuzzleDlc> dlcData;
  LevelsMenu(this.dlcData);

  @override
  Widget build(BuildContext context) {
    return 
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 25, top: 20),
              child: Text('LevelPacks', style: TextStyle(fontSize: 35, color: Colors.grey),)),
            Container(
              
              height: MediaQuery.of(context).size.height * 0.6,
              child: ListView.builder(
                  padding: EdgeInsets.only(left:  MediaQuery.of(context).size.width * 0.025,right: MediaQuery.of(context).size.width * 0.125),
                  scrollDirection: Axis.horizontal,
                  itemCount: dlcData.length,
                  itemBuilder: (BuildContext context, index) {
                    return LevelPack(dlcData, index);
                  }),
            ),
          ],
        );
     
    
  }
}
