import 'package:flutter/material.dart';

import '../widgets/calendar.dart';

class AccountMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.32,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [
                      Color(0XFF1D976C),
                      Color(0XFF71B280),
                    ])),
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(0),
                      margin: EdgeInsets.all(0),
                      width: MediaQuery.of(context).size.width * 0.20,
                      height: MediaQuery.of(context).size.height * 0.20,
                      decoration: new BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 3),
                              blurRadius: 10,
                              color: Colors.black12,
                            )
                          ]),
                      child: Center(
                        child: Text('A',
                            style: TextStyle(
                                fontSize: 50,
                                color: Colors.green,
                                fontWeight: FontWeight.w600)),
                      ),
                    ),
                    Text(
                      'example@gmail.com',
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.white70,
                          fontWeight: FontWeight.w400),
                    ),
                    Text(
                      'UserName',
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.topCenter,
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * .30,
                ),
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.03,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(0, -4),
                        blurRadius: 3,
                        color: Colors.black12,
                      )
                    ],
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15),
                    ),
                    color: Colors.white,
                  ),
                ),
              )
            ],
          ),
          Container(
            margin: EdgeInsets.all(20),
            child: Text(
              'Daily Activity',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 25,
                color: Colors.grey[700],
              ),
            ),
          ),
          DailyActivityChart(),
          Container(
            margin: EdgeInsets.all(20),
            child: Text(
              'Puzzle Completed',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 25,
                color: Colors.grey[700],
              ),
            ),
          ),
          Container(
              //puzzle % completed
              width: MediaQuery.of(context).size.width * 0.87,
              height: MediaQuery.of(context).size.height * 0.18,
              padding: EdgeInsets.all(25),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(0, 0),
                      blurRadius: 10,
                      spreadRadius: 2,
                      color: Colors.black12,
                    )
                  ]),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Solved',
                        style: TextStyle(fontSize: 18),
                      ),
                      Text(
                        '14',
                        style: TextStyle(fontSize: 20),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        '% Solved',
                        style: TextStyle(fontSize: 18),
                      ),
                      Text(
                        '22%',
                        style: TextStyle(fontSize: 20),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Score',
                        style: TextStyle(fontSize: 18),
                      ),
                      Text(
                        '145',
                        style: TextStyle(fontSize: 20),
                      ),
                    ],
                  )
                ],
              )),
          Container(
            margin: EdgeInsets.all(20),
            child: Text(
              'Badges Earned',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 25,
                color: Colors.grey[700],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 20),
            //badges - achievements
            width: MediaQuery.of(context).size.width * 0.9,
            height: MediaQuery.of(context).size.height * 0.25,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 0),
                    blurRadius: 10,
                    spreadRadius: 2,
                    color: Colors.black12,
                  )
                ]),
          ),
        ],
      ),
    );
  }
}
