import 'package:flutter/material.dart';

import 'package:riddly/models/multiple_choice_puzzle.dart';
import 'package:riddly/widgets/daily_puzzle_box.dart';

class TodayMenu extends StatefulWidget {
  TodayMenu(this.puzzleData, this.refresh, this.refreshKey);
  final Function refresh;
  final List<MultipleChoicePuzzle> puzzleData;
  final GlobalKey<RefreshIndicatorState> refreshKey;

  @override
  TodayMenuState createState() => TodayMenuState();
}

class TodayMenuState extends State<TodayMenu> {
  static const String dailyDlcId = 'db3a9d60-3f36-4e30-b5da-8d6f9d49baf8';

  List<MultipleChoicePuzzle> dailyPuzzleList = [];
  @override
  void initState() {
    for (int x = 0; x < widget.puzzleData.length; x++) {
      if (widget.puzzleData[x].dlcid == dailyDlcId) {
        dailyPuzzleList.add(widget.puzzleData[x]);
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      color: Colors.green,
      key: widget.refreshKey,
      onRefresh: widget.refresh,
      child: ListView.builder(
          physics: const AlwaysScrollableScrollPhysics(),
          itemCount: dailyPuzzleList.length,
          itemBuilder: (BuildContext context, index) {
            return Container(
              child: DailyBox(dailyPuzzleList, index),
            );
          }),
    );
  }
}
