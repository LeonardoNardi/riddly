import 'package:flutter/material.dart';
import 'package:riddly/models/multiple_choice_puzzle.dart';
import 'package:riddly/widgets/answer_output.dart';
import 'dart:math';

import '../widgets/hint_button.dart';
import '../widgets/answer_button.dart';

class PuzzlePage extends StatefulWidget {
  final MultipleChoicePuzzle puzzle;
  PuzzlePage(this.puzzle);

  @override
  PuzzlePageState createState() => PuzzlePageState();
}

class PuzzlePageState extends State<PuzzlePage> {

  List answersShuffle;
  MultipleChoicePuzzle puzzleData;
  
  int pressedButton;
  String answerSelected;

  Border borderStyle = Border.all(color: Colors.green);
  Color colorResult;
  String answerOutput;

  final TextStyle textSettings = TextStyle(
      fontSize: 17, fontWeight: FontWeight.w400, color: Colors.grey[600]);
  final TextStyle titleTextSettings = TextStyle(
      fontSize: 25, fontWeight: FontWeight.normal, color: Colors.grey[600]);

List shuffle(List items) {
  var random = new Random();

  // Go through all elements.
  for (var i = items.length - 1; i > 0; i--) {

    // Pick a pseudorandom number according to the list length
    var n = random.nextInt(i + 1);

    var temp = items[i];
    items[i] = items[n];
    items[n] = temp;
  }

  return items;
}

  void answerResult(String output, Color color) {
    setState(() {
      colorResult = color;
      answerOutput = output;
    });
  }

  void buttonSelected(int index) {
    setState(() {
      pressedButton = index;
      answerSelected = answersShuffle[pressedButton];
    });
  }

  @override
  void initState() {
    puzzleData = widget.puzzle;
    answersShuffle = shuffle(puzzleData.correctAnswers + puzzleData.uncorrectAnswers);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(35),
                bottomRight: Radius.circular(35))),
        backgroundColor: Colors.green[800], //aggiungere gradiente
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.more_vert),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  Text(
                    puzzleData.name,
                    style: titleTextSettings,
                  ),
                  Text(
                    puzzleData.description,
                    style: textSettings,
                  ),
                ],
              ),
            ),

            Container(
              height: MediaQuery.of(context).size.height *
                  (0.1 *
                      (puzzleData.uncorrectAnswers.length +
                          puzzleData.correctAnswers.length)),
              width: MediaQuery.of(context).size.width * 0.85,
              child: ListView.builder(
                  primary: false,
                  reverse: false,
                  itemCount: puzzleData.uncorrectAnswers.length +
                      puzzleData.correctAnswers.length,
                  itemBuilder: (context, index) {
                    return Container(
                      margin: EdgeInsets.all(5),
                      width: MediaQuery.of(context).size.width * 0.65,
                      height: MediaQuery.of(context).size.height * 0.08,
                      decoration: BoxDecoration(
                          border: (index == pressedButton)
                              ? borderStyle
                              : Border.all(color: Colors.transparent),
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 0),
                              blurRadius: 8,
                              color: Colors.black12,
                            )
                          ]),
                      child: FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          onPressed: () => buttonSelected(index),
                          child: Row(
                            children: <Widget>[
                              Text('${index + 1} ' + ':  '),
                              Text(answersShuffle[index]),
                            ],
                          )),
                    );
                  }),
            ),
            (answerOutput == null) ? Container() : AnswerOutput(answerOutput, colorResult),
            //hint and answer button
            Container(
              margin: EdgeInsets.only(top: 25, bottom: 25),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  HintButton(),
                  AnswerButton(puzzleData.correctAnswers, answerSelected, answerResult)
                  //check this button
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
