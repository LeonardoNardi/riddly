import 'package:riddly/models/puzzle.dart';

class MultipleChoicePuzzle extends Puzzle {
  String id;
  String dlcId;

  int created;

  String name;
  String description;

  List<String> correctAnswers;
  List<String> uncorrectAnswers;

  int get date {
    return created;
  }

  String get dlcid {
    return dlcId;
  }

  MultipleChoicePuzzle({this.id, this.dlcId, this.created, this.name, this.description,
      this.correctAnswers, this.uncorrectAnswers});  
}
