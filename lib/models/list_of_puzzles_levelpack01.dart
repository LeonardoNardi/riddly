class PuzzleList {
  static final puzzleList = [
    {
      'id': 1,
      'title': 'DICE ROLL',
      'question': 'A young boy sits quietly on a stoop rolling a single die over and over. Each time the die stops rolling, he picks it up, examines it, and whispers something to himself. Each time he rolls a five, he whispers, "19". "Each time he gets a two, he whispers, "16." The boy has just rolled a four. What number will he whisper this time?',
      'answers': [
        'He whispers "17"',
        'He whispers "14"',
        'He whispers "21"',
        'He whispers "18"',
      ],
      'correct': 3,
    },
    {
      'id': 2,
      'title': 'adasdasd',
      'question': 'question',
      'answers': [
        'answer1',
        'answer2',
        'answer3',
        'answer4',
      ],
      'correct': 3,
    }
  ];
}
