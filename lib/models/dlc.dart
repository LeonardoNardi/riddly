class PuzzleDlc {
  int created;
  String description;
  String dlcId;
  String name;
  int price;

  PuzzleDlc({this.created, this.description, this.dlcId, this.name, this.price});
}