import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';

class DailyActivityChart extends StatefulWidget {
  @override
  _DailyActivityChartState createState() => _DailyActivityChartState();
}

class _DailyActivityChartState extends State<DailyActivityChart> {
  CalendarController _controller;
  final Map<CalendarFormat, String> availableCalendarFormats = const {
    CalendarFormat.week: 'Week',
  };

  @override
  void initState() {
    super.initState();
    _controller = CalendarController();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.22,
      width: MediaQuery.of(context).size.width * 0.87,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 0),
              blurRadius: 10,
              spreadRadius: 2,
              color: Colors.black12,
            )
          ]),
      child: Align(
        alignment: Alignment.center,
        child: TableCalendar(
          calendarStyle: CalendarStyle(
            weekendStyle: TextStyle(color:Colors.black),
            holidayStyle: TextStyle(color: Colors.black),
            todayColor: Colors.transparent,
            todayStyle: TextStyle(color: Colors.black),
            selectedColor: Colors.transparent,
            selectedStyle: TextStyle(color: Colors.black),
          ),
          headerStyle: HeaderStyle(
            centerHeaderTitle: true,
          ),
          locale: 'en_US',
          availableGestures: AvailableGestures.none,
          calendarController: _controller,
          initialCalendarFormat: CalendarFormat.week,
          availableCalendarFormats: availableCalendarFormats,
        ),
      ),
    );
  }
}
