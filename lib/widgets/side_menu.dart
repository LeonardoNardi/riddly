import 'package:flutter/material.dart';

class SideMenu extends StatelessWidget {
  final TextStyle textSettings = (TextStyle(
    fontSize: 20,
    color: Colors.white,
    fontWeight: FontWeight.bold,
  ));

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Color(0xff1e272e),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 10),
              height: MediaQuery.of(context).size.height * 0.2,
              width: MediaQuery.of(context).size.width * 0.4,
              child: Image.asset(
                'assets/images/Path_2.png',
              ),
            ),
            Container(
              color: Color(0xFF1e272e),
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: Icon(
                      Icons.info_outline,
                      color: Colors.white,
                    ),
                    title: Text(
                      'About Riddly',
                      style: textSettings,
                    ),
                    onTap: () {},
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.mail_outline,
                      color: Colors.white,
                    ),
                    title: Text(
                      'Contact Us',
                      style: textSettings,
                    ),
                    onTap: () {},
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.favorite,
                      color: Colors.white,
                    ),
                    title: Text(
                      'Support',
                      style: textSettings,
                    ),
                    onTap: () {},
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.star,
                      color: Colors.white,
                    ),
                    title: Text(
                      'Rate us',
                      style: textSettings,
                    ),
                    onTap: () {},
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.exit_to_app,
                      color: Colors.white,
                    ),
                    title: Text(
                      'Log out',
                      style: textSettings,
                    ),
                    onTap: () {},
                  ),
                  Divider(
                    color: Colors.green[300],
                    height: 4,
                    thickness: 2,
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Column(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: 65),
                              alignment: Alignment.centerLeft,
                              width: double.infinity,
                              child: FlatButton(
                                child: Text('Privacy policy',style: TextStyle(color: Color(0x88ffffff)),),
                                onPressed: () {},
                              ),
                            ),
                            Container(
                              alignment: Alignment.centerLeft,
                              width: double.infinity,
                                child: FlatButton(
                              child: Text('Terms of use', style: TextStyle(color: Color(0x88ffffff),),),
                              onPressed: () {},
                            )),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: 25),
                                child: Text(
                                  'Version 0.0.1',
                                  style: TextStyle(color: Color(0x88ffffff),),
                                ),
                              ),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
