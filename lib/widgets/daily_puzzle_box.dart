import 'package:flutter/material.dart';
import 'package:riddly/models/multiple_choice_puzzle.dart';


import '../screens/puzzle_page.dart';

class DailyBox extends StatelessWidget {

 final List<MultipleChoicePuzzle> puzzleList;
 final int index;

  DailyBox(this.puzzleList, this.index);
  @override
  Widget build(BuildContext context) {

    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.green[100]),
        borderRadius: BorderRadius.circular(15),
        color: Colors.white,
        boxShadow: [BoxShadow(
          offset: Offset(0, 0),
          color: Colors.black12,
          blurRadius: 5,
        )]
      ),
      margin: EdgeInsets.only(top:20, left: 15, right: 15,bottom: 15),
      
      child: Container(
        padding: EdgeInsets.all(15),
        width: 325,
        height: 225,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              puzzleList[index].name,
              style: TextStyle(
                fontSize: 30,
                color: Colors.grey,
              ),
            ),
            Text(
              puzzleList[index].description,
              style: TextStyle(),
            ),
            Container(
              child: Column(
                children: <Widget>[
                  Divider(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          IconButton(
                            onPressed: () {},
                            icon: Icon(Icons.share),
                            color: Colors.green,
                            iconSize: 30,
                          ),
                        ],
                      ),
                      FlatButton(
                        color: Colors.transparent,
                        padding: const EdgeInsets.all(0.0),
                        textColor: Colors.white,
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => PuzzlePage(puzzleList[index])));
                        },
                        child: Container(
                          margin: EdgeInsets.only(right: 10),
                          padding: const EdgeInsets.only(
                            left: 25,
                            right: 25,
                            top: 10,
                            bottom: 10,
                          ),
                          child: Text(
                            'Go Solve!',
                            style: TextStyle(fontSize: 15),
                          ),
                          decoration: const BoxDecoration(
                            color: Color(0xFF1BB27F),
                            borderRadius: BorderRadius.all(Radius.circular(6)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
