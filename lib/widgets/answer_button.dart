import 'package:flutter/material.dart';

class AnswerButton extends StatelessWidget {
  final List<String> correct;
  final String selected;
  final Function answerResult;
  AnswerButton(this.correct, this.selected, this.answerResult);

  void checkAnswer() {
    if (selected == null) {
      answerResult('First choose an answer!', Colors.grey[400]);
    } else {
      if (selected == correct[0]) {
        answerResult('Correct!', Colors.green[300]);
      } else {
       answerResult('Wrong! Try again...', Colors.red[300]);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      elevation: 1,
      padding: EdgeInsets.only(left: 25, right: 25,top: 15,bottom: 15),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10))),
      onPressed: () => checkAnswer(),
      color: Colors.green[400],
      child: Text(
        'Answer!',
        style: TextStyle(color: Colors.white, fontSize: 24),
      ),
    );
  }
}
