import 'package:flutter/material.dart';

class HintButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      elevation: 1,
      padding: EdgeInsets.only(left: 45, right: 45,top: 15,bottom: 15),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10))),
      onPressed: () {},
      color: Colors.orange[300],
      child: Text(
        'Hint',
        style: TextStyle(color: Colors.white, fontSize: 24),
      ),
    );
  }
}
