import 'package:flutter/material.dart';

class AnswerOutput extends StatelessWidget {
  final String output;
  final Color color;
  AnswerOutput(this.output, this.color);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      width: MediaQuery.of(context).size.width * 0.8,
      height: MediaQuery.of(context).size.height * 0.1,
      decoration:
          BoxDecoration(color: color, borderRadius: BorderRadius.circular(10)),
      child: Center(
          child: Text(
        output,
        style: TextStyle(color: Colors.white, fontSize: 22),
      )),
    );
  }
}
