import 'package:flutter/material.dart';
import 'package:riddly/models/dlc.dart';
import 'package:riddly/models/multiple_choice_puzzle.dart';
import './screens/account_menu.dart';
import './screens/levels_menu.dart';

import './Providers/puzzles_provider.dart';
import './screens/today_menu.dart';
import './widgets/side_menu.dart';



class Riddly extends StatefulWidget {
  @override
  _RiddlyState createState() => _RiddlyState();
}

class _RiddlyState extends State<Riddly> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  List<Widget> selectPage;
  bool loaded = false;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  int selectedIndex = 0;

  List<MultipleChoicePuzzle> puzzleLoader;
  List<PuzzleDlc> dlcLoader;

  Future<void> loader() async {
    puzzleLoader = await fetchLevelData();
    dlcLoader = await fetchDlcData();
    loaded = true;
  }

  Future<void> setLoadBool() async {
    loaded = false;
  }

  Future<Null> refresh() {
    return setLoadBool().then((_) {
      setState(() {
        selectPage[selectedIndex] = selectPage[0];
      });
    });
  }

  @override
  void initState() {
    //convert dateTime to timestamp...
    //var date3 = DateTime.now().toUtc().millisecondsSinceEpoch;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primarySwatch: Colors.green,
          accentColor: Colors.blue,
          textTheme: TextTheme(
              title: TextStyle(
            color: Colors.green[800],
          ))),
      home: Scaffold(
        backgroundColor: Color(0xfffefefe),
        key: _scaffoldKey,
        appBar: AppBar(
            centerTitle: true,
            backgroundColor: Colors.white,
            elevation: 5,
            title: Container(
              width: 100,
              child: Image.asset('assets/images/Path_1.png'),
            ),
            leading: IconButton(
              icon: Icon(
                Icons.menu,
                color: Colors.black54,
                size: 35,
              ),
              onPressed: () {
                _scaffoldKey.currentState.openDrawer();
              },
            )),
        drawer: SideMenu(),
        body: (loaded == true)
            ? selectPage[selectedIndex]
            : FutureBuilder(
                future: loader(),
                builder: (BuildContext context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return Center(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          CircularProgressIndicator(),
                          Container(
                              margin: EdgeInsets.all(10),
                              child: Text('Loading...'))
                        ],
                      ));
                    default:
                      if (snapshot.hasError)
                        return RefreshIndicator(
                          onRefresh: refresh,
                          key: _refreshIndicatorKey,
                          child: Center(
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.9,
                              height: MediaQuery.of(context).size.height * 0.6,
                              child: ListView(
                                padding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.1),
                                children: <Widget>[
                                  Icon(Icons.error_outline,
                                      size: 150, color: Colors.grey[400]),
                                  Padding(
                                    padding: const EdgeInsets.all(12),
                                    child: Text(
                                      'Error: couldn\'t retrieve data from server. Please check your internet connection. {$snapshot.hasError})',
                                      style: TextStyle(
                                          fontSize: 25,
                                          color: Colors.grey[500]),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      else
                        selectPage = <Widget>[
                          TodayMenu(
                              puzzleLoader, refresh, _refreshIndicatorKey),
                          LevelsMenu(dlcLoader),
                          AccountMenu(),
                        ];

                      return selectPage[selectedIndex];
                  }
                }),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.today),
              title: Text('Today'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.apps),
              title: Text('Levels'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: Text('Account'),
            ),
          ],
          currentIndex: selectedIndex,
          onTap: (int index) {
            setState(() {
              selectedIndex = index;
            });
          },
        ),
      ),
    );
  }
}
